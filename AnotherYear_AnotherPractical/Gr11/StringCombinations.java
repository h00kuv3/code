package Gr11;

/*Write a program in Java to accept a four-letter word. Display all the
probable four letter combinations such that no letter should be
repeated in the output within each combination.*/

import java.util.Scanner;

public class StringCombinations {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String word;
        do {
            System.out.print("Enter the word: ");
            word = in.next();
            if (word.length() != 4) {
                System.out.print("Invalid Input! Please enter a four letter word: ");
            }
        } while (word.length() != 4);
        for (int i = 0; i < word.length(); i++) {
            for (int j = 0; j < word.length(); j++) {
                for (int k = 0; k < word.length(); k++) {
                    for (int l = 0; l < word.length(); l++) {
                        if (!(i == j || i == k || i == l || j == k || j == l || k == l)) {
                            System.out.print(word.charAt(i));
                            System.out.print(word.charAt(j));
                            System.out.print(word.charAt(k));
                            System.out.println(word.charAt(l));
                        }
                    }
                }
            }
        }
    }
}

package Gr11;

/*Write a program to input and store n integers (n > 0) in a single
subscript variable and print each number with its frequency. The
output should contain number and its frequency in two different
columns.*/

import java.util.Scanner;

public class SDA_Frequency {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print("Enter the number of elements (n): ");
            n = in.nextInt();
            if (n <= 0) {
                System.out.print("Invalid Input! Enter the value again\n\n");
            }
        } while (n <= 0);
        int[] arr = new int[n];
        sda_input(arr, arr.length - 1);
        sda_frequency(arr, arr.length - 1);
    }

    static void sda_frequency(int[] a, int end) {
        sda_insort(a, a.length - 1);
        int i, c;
        System.out.println("Number\tFrequency");
        for (i = 0, c = 1; i < end; i++) {
            if (a[i] == a[i + 1]) {
                ++c;
            } else {
                System.out.println(a[i] + "\t\t\t" + c);
                c = 1;
            }
        }
        System.out.println(a[i] + "\t\t\t" + c);
    }

    static void sda_insort(int[] a, int end) {
        for (int i = 1; i < (end + 1); i++) {
            for (int j = i; j > 0; j--) {
                if ((a[j] < a[j - 1])) {
                    sda_switch(a, j - 1, j);
                }
            }
        }
    }

    static void sda_switch(int[] a, int i, int j) {
        a[i] = a[i] + a[j];
        a[j] = a[i] - a[j];
        a[i] = a[i] - a[j];
    }

    static void sda_input(int[] a, int end) {
        Scanner in = new Scanner(System.in);
        end = Math.min(a.length - 1, end);
        System.out.println("Enter the elements: ");
        for (int i = 0; i <= end; i++) {
            a[i] = in.nextInt();
        }
    }
}

package Gr11;

/*A square matrix is the matrix in which number of rows equals the
number of columns. Thus, a matrix of order n*n is called a Square Matrix.
Write a program in Java to create a double dimensional array of size
nxn matrix form and fill the numbers in a circular fashion (anticlockwise)
with natural numbers from 1 to n^2 taking n as an input. The filling of the
elements should start from outer to the central cell.*/

import java.util.Scanner;

public class Anticlockwise_Arrange {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print("Enter the order (n): ");
            n = in.nextInt();
            if (n < 1) {
                System.out.print("Invalid Input! Enter the values again\n\n");
            }
        } while (n < 1);
        int[][] a = new int[n][n];
        anticlockwise(a, n);
        dda_display(a, n, n);
    }

    static void anticlockwise(int[][] ar, int n) {
        System.out.print("");
        int a = 0;
        int b = n - 1;
        int c = 1;
        while (a < n) {
            for (int i = a; i <= b; i++) {
                ar[i][a] = c++;
            }
            a++;
            for (int i = a; i <= b; i++) {
                ar[b][i] = c++;
            }
            for (int i = b - 1; i >= a - 1; i--) {
                ar[i][b] = c++;
            }
            b--;
            for (int i = b; i >= a; i--) {
                ar[a - 1][i] = c++;
            }
        }
    }

    static void dda_display(int[][] a, int r, int c) {
        System.out.print("The array arranged in anticlockwise order is:\n");
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                System.out.print(a[i][j] + "\t");
            }
            System.out.println();
        }
    }
}

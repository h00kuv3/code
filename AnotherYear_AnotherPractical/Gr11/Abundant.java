package Gr11;

/*Accept two positive integers ‘m’ and ‘n’, where m is less than n as user input. Display the
number of abundant integers that are in the range between ‘m’ and ‘n’ (both inclusive)
and output them along with the frequency,*/

import java.util.Scanner;

public class Abundant {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int m, n, c, i;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print("Enter the lower limit (m): ");
            m = in.nextInt();
            System.out.print("Enter the upper limit (n): ");
            n = in.nextInt();
            if (m >= n || m < 1) {
                System.out.print("Invalid Input! Enter the values again\n\n");
            }
        } while (m >= n || m < 1);
        System.out.println("The Abundant Numbers are: ");
        for (i = m, c = 0; i <= n; i++) {
            if (isAbundant(i)) {
                ++c;
                System.out.print(i + ", ");
            }
        }
        if (c == 0) System.out.print("None!  ");
        System.out.println("\b\b\nFrequency: " + c); //To remove the extra commas (,) at the end and display the frequency
    }

    static boolean isAbundant(int n) {
        return aliquot_sum(n) > n;
    }

    static int aliquot_sum(int n) {
        int S = 0;
        for (int i = 1; i <= (n / 2); i++) {
            S += (n % i == 0) ? i : 0;
        }
        return S;
    }
}

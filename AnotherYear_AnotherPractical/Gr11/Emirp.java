package Gr11;

/*An Emirp number is a number which is prime when it is reversed also.
For example 13,31,17,71,79,97 are some examples of emirp number
Given two positive integers m and n, where m < n, write a
program to determine how many Emirp integers are there in the
range between m and n (both inclusive) and output them.
The input contains two positive integers m and n where m < 5000
and n < 5000. Display the number of EMIRP integers in the
specified range along with their values*/

import java.util.Scanner;

public class Emirp {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int m, n, c, i;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print("Enter the lower limit (m): ");

            m = in.nextInt();
            System.out.print("Enter the upper limit (n): ");
            n = in.nextInt();
            if (m >= n || m >= 5000 || n >= 5000 || m < 1) {
                System.out.print("Invalid Input! Enter the values again\n\n");
            }
        } while (m >= n || m >= 5000 || n >= 5000 || m < 1);
        System.out.println("The Emirp Numbers are: ");
        for (i = m, c = 0; i <= n; i++) {
            if (isPrime(i) && isPrime(reverse(i))) {
                ++c;
                System.out.print(i + ", ");
            }
        }
        if (c == 0) System.out.print("None!  ");
        System.out.println("\b\b\nFrequency: " + c); //To remove the extra commas (,) at the end and display the frequency
    }

    static boolean isPrime(int n) { //Function to check whether n is a prime number or not
        int c = 0;
        if (n == 1 || n == 4) {
            return false;
        } else if (n == 2) {
            return true;
        } else {
            for (int i = 2; i < (n / 2); i++) {
                if (n % i == 0) {
                    ++c;
                }
            }
        }
        return c == 0;
    }

    static int reverse(int n) { // Function to reverse n and return the reversed number
        int reverse = 0;
        for (; n > 0; n /= 10) {
            reverse = reverse * 10 + (n % 10);
        }
        return reverse;
    }
}

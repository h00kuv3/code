package Gr11;

/*A company manufactures packing cartons in four sizes, i.e. cartons
to accommodate 6 boxes, 12 boxes, 24 boxes and 48 boxes. Design
a program to accept the number of boxes to be packed (N) by the
user (maximum up to 1000 boxes) and display the break-up of the
cartons used in descending order of capacity (i.e. preference should
be given to the highest capacity available, and if boxes left are less
than 6, an extra carton of capacity 6 should be used.)*/

import java.util.Scanner;

public class CartonBoxes {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[] box = new int[]{48, 24, 12, 6};
        int n, c, i, cn;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print("Enter the number of boxes (n): ");
            n = in.nextInt();
            if (1 > n || n > 1000) {
                System.out.print("Invalid Input! Enter the values again\n\n");
            }
        } while (1 > n || n > 1000);
        System.out.println("The arrangement is: ");
        for (i = 0, c = 0, cn = n; i < 4; n %= box[i], i++) {
            if (n / box[i] > 0) {
                c += n / box[i];
                System.out.println(box[i] + " * " + n / box[i] + " = " + (box[i] * (n / box[i])));
            }
        }
        System.out.println("Remaining Boxes: " + n + "\nTotal Number of Boxes: " + cn + "\nTotal Number of Cartons: " + (c + Math.max(n, 1)));
    }
}

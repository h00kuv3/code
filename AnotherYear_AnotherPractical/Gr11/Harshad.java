package Gr11;

/*Write a Java program to check whether a number is a Harshad
Number or not. In recreational mathematics, a harshad number in a
given number base, is an integer that is divisible by the sum of its
digits when written in that base.*/

import java.util.Scanner;

public class Harshad {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int m, n, c, i;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print("Enter the lower limit (m): ");
            m = in.nextInt();
            System.out.print("Enter the upper limit (n): ");
            n = in.nextInt();
            if (m >= n || m < 1) {
                System.out.print("Invalid Input! Enter the values again\n\n");
            }
        } while (m >= n || m < 1);
        System.out.println("The Harshad Numbers are: ");
        for (i = m, c = 0; i <= n; i++) {
            if (isHarshad(i)) {
                ++c;
                System.out.print(i + ", ");
            }
        }
        if (c == 0) System.out.print("None!  ");
        System.out.print("\b\b\nFrequency: " + c); //To remove the extra commas (,) at the end and display the frequency
    }

    static boolean isHarshad(int n) {
        return n % sumofdigits(n) == 0;
    }

    static int sumofdigits(int n) {
        int sumofdigits = 0;
        for (; n > 0; n /= 10) {
            sumofdigits += (n % 10);
        }
        return sumofdigits;
    }
}

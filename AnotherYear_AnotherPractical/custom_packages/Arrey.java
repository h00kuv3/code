package custom_packages;

import java.util.Scanner;

public class Arrey {
    public static void main(String[] args) {

    }

    public static void fill(int[] a, int n) {
        fill(a, n, 0, a.length - 1);
    }

    public static void fill(int[] a, int n, int end) {
        fill(a, n, 0, end);
    }

    public static void fill(int[] a, int n, int start, int end) {
        end = Math.min(a.length - 1, end);
        if (end < start) {
            start = start + end;
            end = start - end;
            start = start - end;
        }
        for (int i = start; i < (end + 1); i++) {
            a[i] = n;
        }
    }

    public static void fill(boolean[] a, boolean n) {
        fill(a, n, 0, a.length - 1);
    }

    public static void fill(boolean[] a, boolean n, int end) {
        fill(a, n, 0, end);
    }

    public static void fill(boolean[] a, boolean n, int start, int end) {
        end = Math.min(a.length - 1, end);
        if (end < start) {
            start = start + end;
            end = start - end;
            start = start - end;
        }
        for (int i = start; i < (end + 1); i++) {
            a[i] = n;
        }
    }

    public static int l_search(int[] a, int n) {
        return l_search(a, n, 0, a.length - 1);
    }

    public static int l_search(int[] a, int n, int end) {
        return l_search(a, n, 0, end);
    }

    public static int l_search(int[] a, int n, int start, int end) {
        end = Math.min(a.length - 1, end);
        if (end < start) {
            start = start + end;
            end = start - end;
            start = start - end;
        }
        for (int i = start; i < end + 1; i++) {
            if (a[i] == n) {
                return i;
            }
        }
        return -1;
    }

    public static int b_search(int[] a, int n) {
        return b_search(a, n, 0, a.length - 1);
    }

    public static int b_search(int[] a, int n, int end) {
        return b_search(a, n, 0, end);
    }

    public static int b_search(int[] a, int n, int start, int end) {
        sort(a);
        end = Math.min(a.length - 1, end);
        if (end < start) {
            start = start + end;
            end = start - end;
            start = start - end;
        }
        return b_search(a, n, -1, start, end);
    }

    public static int b_search(int[] a, int n, int error, int start, int end) {
        int mid = (start + end) / 2;
        if (n == a[mid]) {
            return mid;
        } else if (n < a[mid]) {
            return b_search(a, n, error, start, mid - 1);
        } else if (n > mid) {
            return b_search(a, n, error, mid + 1, end);
        }
        return error;
    }

    public static void sort(int[] a) {
        h_sort(a, true);
    }

    public static void sort(int[] a, boolean ascending) {
        h_sort(a, ascending);
    }

    public static void sort(int[] a, String descending) {
        h_sort(a, false);
    }

    public static void sort(int[] a, int end) {
        sort(a, 0, end, true);
    }

    public static void sort(int[] a, int end, boolean ascending) {
        sort(a, 0, end, ascending);
    }

    public static void sort(int[] a, int end, String descending) {
        sort(a, 0, end, false);
    }

    public static void sort(int[] a, int start, int end) {
        sort(a, start, end, true);
    }

    public static void sort(int[] a, int start, int end, String descending) {
        sort(a, start, end, false);
    }

    public static void sort(int[] a, int start, int end, boolean ascending) {
        end = Math.min(a.length - 1, end);
        if (end < start) {
            start = start + end;
            end = start - end;
            start = start - end;
        }
        if (start < end) {
            int middle = (end + start) / 2; // Find the middle point
            sort(a, start, middle, ascending); // Sort first half
            sort(a, middle + 1, end, ascending); //Sort second half
            merge(a, start, middle, end); // Merge the sorted halves
        }
    }

    private static void merge(int[] a, int start, int middle, int end) {
        int n1 = middle - start + 1, n2 = end - middle; //Sizes of the subarrays
        int[] L = new int[n1], R = new int[n2]; //Temp Arrays
        System.arraycopy(a, start, L, 0, n1); //Manual Array Copy
        System.arraycopy(a, middle + 1, R, 0, n2);
        int i = 0, j = 0, k = start; //Initial indices
        for (; i < n1 && j < n2; k++) { //Merge the temp arrays
            if (L[i] <= R[j]) {
                a[k] = L[i++];
            } else {
                a[k] = R[j++];
            }
        }
        for (; i < n1; a[k++] = L[i++]) {
        } //Fill the remaining elements
        for (; j < n2; a[k++] = R[j++]) {
        }
    }

    private static void h_sort(int[] a, boolean ascending) {
        int n = a.length;
        for (int i = n / 2 - 1; i >= 0; i--) {
            heapify(a, n, i, ascending);
        }
        for (int i = n - 1; i >= 0; i--) {
            swap(a, i, 0);
            heapify(a, i, 0, ascending);
        }
    }


    private static void heapify(int[] a, int n, int i, boolean ascending) {
        int largest = i, l = 2 * i + 1, r = 2 * i + 2;
        if (l < n && (ascending ? (a[l] > a[largest]) : (a[l] < a[largest]))) {
            largest = l;
        }
        if (r < n && (ascending ? (a[r] > a[largest]) : (a[r] < a[largest]))) {
            largest = r;
        }
        if (largest != i) {
            swap(a, i, largest);
            heapify(a, n, largest, ascending);
        }
    }

    public static void swap(int[] a, int i, int j) {
        int t = a[i];
        a[i] = a[j];
        a[j] = t;
    }

    public static void print(int[] a) {
        print(a, 0, a.length - 1, "", ',');
    }

    public static void print(int[] a, String print) {
        print(a, 0, a.length - 1, print, ',');
    }

    public static void print(int[] a, char separator) {
        print(a, 0, a.length - 1, "", separator);
    }

    public static void print(int[] a, String print, char separator) {
        print(a, 0, a.length - 1, print, separator);
    }

    public static void print(int[] a, int end) {
        print(a, 0, end, "", ',');
    }

    public static void print(int[] a, int end, String print) {
        print(a, 0, end, print, ',');
    }

    public static void print(int[] a, int end, char separator) {
        print(a, 0, end, "", separator);
    }

    public static void print(int[] a, int end, String print, char separator) {
        print(a, 0, end, print, separator);
    }

    public static void print(int[] a, int start, int end) {
        print(a, start, end, "", ',');
    }

    public static void print(int[] a, int start, int end, String print) {
        print(a, start, end, print, ',');
    }

    public static void print(int[] a, int start, int end, char separator) {
        print(a, start, end, "", separator);
    }

    public static void print(int[] a, int start, int end, String print, char separator) {
        end = Math.min(a.length - 1, end);
        if (end < start) {
            start = start + end;
            end = start - end;
            start = start - end;
        }
        System.out.print(print);
        for (int i = start; i <= end; i++) {
            if (i == start) {
                System.out.print(a[i]);
            } else {
                System.out.print(separator + " " + a[i]);
            }
        }
    }

    public static void print(int[][] a, int r, int c) {
        print(a, 0, 0, r, c, "", "\t");
    }

    public static void print(int[][] a, int r, int c, String display) {
        print(a, 0, 0, r, c, display, "\t");
    }

    public static void print(int[][] a, int r, int c, String display, String separator) {
        print(a, 0, 0, r, c, display, separator);
    }

    public static void print(int[][] a, int sr, int sc, int er, int ec, String display, String separator) {
        System.out.print(display);
        for (int i = sr; i < er; i++) {
            for (int j = sc; j < ec; j++) {
                System.out.print(a[i][j] + "" + separator);
            }
            System.out.println();
        }
    }

    public static void print(char[][] a, int r, int c) {
        print(a, 0, 0, r, c, "", "\t");
    }

    public static void print(char[][] a, int r, int c, String display) {
        print(a, 0, 0, r, c, display, "\t");
    }

    public static void print(char[][] a, int r, int c, String display, String separator) {
        print(a, 0, 0, r, c, display, separator);
    }

    public static void print(char[][] a, int sr, int sc, int er, int ec, String display, String separator) {
        System.out.print(display);
        for (int i = sr; i < er; i++) {
            for (int j = sc; j < ec; j++) {
                System.out.print(a[i][j] + "" + separator);
            }
            System.out.println();
        }
    }

    public static void print(String[][] a, int r, int c) {
        print(a, 0, 0, r, c, "", "\t");
    }

    public static void print(String[][] a, int r, int c, String display) {
        print(a, 0, 0, r, c, display, "\t");
    }

    public static void print(String[][] a, int r, int c, String display, String separator) {
        print(a, 0, 0, r, c, display, separator);
    }

    public static void print(String[][] a, int sr, int sc, int er, int ec, String display, String separator) {
        System.out.print(display);
        for (int i = sr; i < er; i++) {
            for (int j = sc; j < ec; j++) {
                System.out.print(a[i][j] + "" + separator);
            }
            System.out.println();
        }
    }


    public static void input(int[] a) {
        input(a, 0, a.length - 1, "");
    }

    public static void input(int[] a, String print) {
        input(a, 0, a.length - 1, print);
    }

    public static void input(int[] a, int end) {
        input(a, 0, end, "");
    }

    public static void input(int[] a, int end, String print) {
        input(a, 0, end, print);
    }

    public static void input(int[] a, int start, int end) {
        input(a, start, end, "");
    }

    public static void input(int[] a, int start, int end, String print) {
        end = Math.min(a.length - 1, end);
        if (end < start) {
            start = start + end;
            end = start - end;
            start = start - end;
        }
        Scanner in = new Scanner(System.in);
        System.out.print(print);
        for (int i = start; i < (end + 1); i++) {
            a[i] = in.nextInt();
        }
    }

    public static void input(char[] a) {
        input(a, 0, a.length - 1, "");
    }

    public static void input(char[] a, String print) {
        input(a, 0, a.length - 1, print);
    }

    public static void input(char[] a, int end) {
        input(a, 0, end, "");
    }

    public static void input(char[] a, int end, String print) {
        input(a, 0, end, print);
    }

    public static void input(char[] a, int start, int end) {
        input(a, start, end, "");
    }

    public static void input(char[] a, int start, int end, String print) {
        end = Math.min(a.length - 1, end);
        if (end < start) {
            start = start + end;
            end = start - end;
            start = start - end;
        }
        Scanner in = new Scanner(System.in);
        System.out.print(print);
        end = Math.min(a.length - 1, end);
        for (int i = start; i < (end + 1); i++) {
            a[i] = in.next().charAt(0);
        }
    }

    public static void reverse(int[] a) {
        reverse(a, 0, a.length - 1);
    }

    public static void reverse(int[] a, int end) {
        reverse(a, 0, end);
    }

    public static void reverse(int[] a, int start, int end) {
        end = Math.min(a.length - 1, end);
        if (end < start) {
            start = start + end;
            end = start - end;
            start = start - end;
        }
        int[] b = new int[end - start + 1];
        for (int i = end, j = 0; i >= start; i--, j++) {
            b[j] = a[i];
        }
        for (int i = start, j = 0; i < (end + 1); i++, j++) {
            a[i] = b[j];
        }
    }

    public static int max(int[] a) {
        return max(a, 0, a.length - 1);
    }

    public static int max(int[] a, int end) {
        return max(a, 0, end);
    }

    public static int max(int[] a, int start, int end) {
        end = Math.min(a.length - 1, end);
        if (end < start) {
            start = start + end;
            end = start - end;
            start = start - end;
        }
        int max = a[start];
        for (int i = start + 1; i < (end + 1); i++) {
            max = Math.max(max, a[i]);
        }
        return max;
    }

    public static int min(int[] a) {
        return min(a, 0, a.length - 1);
    }

    public static int min(int[] a, int end) {
        return min(a, 0, end);
    }

    public static int min(int[] a, int start, int end) {
        end = Math.min(a.length - 1, end);
        if (end < start) {
            start = start + end;
            end = start - end;
            start = start - end;
        }
        int min = a[start];
        for (int i = start + 1; i < (end + 1); i++) {
            min = Math.min(min, a[i]);
        }
        return min;
    }

    public static void frequency(int[] a) {
        frequency(a, 0, a.length - 1, "");
    }

    public static void frequency(int[] a, String print) {
        frequency(a, 0, a.length - 1, print);
    }

    public static void frequency(int[] a, int end) {
        frequency(a, 0, end, "");
    }

    public static void frequency(int[] a, int end, String print) {
        frequency(a, 0, end, print);
    }

    public static void frequency(int[] a, int start, int end) {
        frequency(a, start, end, "");
    }

    public static void frequency(int[] a, int start, int end, String print) {
        end = Math.min(a.length - 1, end);
        if (end < start) {
            start = start + end;
            end = start - end;
            start = start - end;
        }
        sort(a);
        int i, c;
        System.out.print(print);
        for (i = start, c = 1; i < end; i++) {
            if (a[i] == a[i + 1]) {
                ++c;
            } else {
                System.out.println(a[i] + "\t\t\t" + c);
                c = 1;
            }
        }
        System.out.println(a[i] + "\t\t\t" + c);
    }

    public static int sum(int[] a, int start, int end, String print) {
        end = Math.min(a.length - 1, end);
        if (end < start) {
            start = start + end;
            end = start - end;
            start = start - end;
        }
        int S = 0;
        for (int i = start; i < end + 1; i++) {
            S += a[i];
        }
        return S;
    }
}

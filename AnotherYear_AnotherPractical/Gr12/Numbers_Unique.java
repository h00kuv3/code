package Gr12;

import java.util.Scanner;

public class Numbers_Unique {
    public static void main(String[] args) {
        int m, n;
        do {
            m = input("Enter the lower limit (m): ");
            n = input("Enter the upper limit (n): ");
            if (m >= n)
                System.out.print("Invalid Input! Enter the value(s) again!\n\n");
        } while (m >= n);
        print_Unique(m, n);
    }

    static int input(String display) {
        Scanner in = new Scanner(System.in);
        int n;
        do { // Do-While loop that repeats until user enters a correct input
            System.out.print(display);
            n = in.nextInt();
            if (n < 1)
                System.out.print("Invalid Input! Enter the value(s) again!\n\n");
        } while (n < 1);
        return n;
    }

    static void print_Unique(int lower, int upper) {
        System.out.println("The Unique Numbers are: ");
        int i, c;
        for (i = lower, c = 0; i < upper + 1; i++)
            if (is_Unique(i))
                if (++c == 1)
                    System.out.print(i);
                else
                    System.out.print(", " + i);
        if (c == 0)
            System.out.println("None!  ");
        else
            System.out.println("\nFrequency: " + c);
    }

    static boolean is_Unique(int n) {
        int[] a = new int[no_digits(n)];
        for (int i = 0; i < a.length; n /= 10, i++)
            a[i] = n % 10;
        sort(a);
        for (int i = 0; i < a.length - 1; i++)
            if (a[i] == a[i + 1])
                return false;
        return true;
    }

    static int no_digits(int n) {
        n = Math.abs(n);
        for (int i = 1; ; i++)
            if (n < (int) (Math.pow(10, i)))
                return i;
    }

    static void sort(int[] a) {
        for (int i = 1; i < a.length; i++)
            for (int j = i; j > 0; j--)
                if (a[j] < a[j - 1])
                    swap(a, j - 1, j);
    }

    static void swap(int[] a, int i, int j) {
        a[i] = a[i] + a[j];
        a[j] = a[i] - a[j];
        a[i] = a[i] - a[j];
    }
}

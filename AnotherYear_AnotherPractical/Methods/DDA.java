package Methods;

import java.util.Scanner;

public class DDA {

    //DDA input: Integer
    static void dda_input(int[][] a, int r, int c, String display) {
        Scanner in = new Scanner(System.in);
        System.out.print(display);
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                a[i][j] = in.nextInt();
            }
        }
    }

    //DDA input: Character
    static void dda_input(char[][] a, int r, int c, String display) {
        Scanner in = new Scanner(System.in);
        System.out.print(display);
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                a[i][j] = in.next().charAt(0);
            }
        }
    }

    static void dda_display(int[][] a, int r, int c, String display) {
        System.out.print(display);
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                System.out.print(a[i][j] + "\t");
            }
            System.out.println();
        }
    }

    static void dda_display(int[][] a, int r, int c, String display, String separator) {
        System.out.print(display);
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                System.out.print(a[i][j] + "" + separator);
            }
            System.out.println();
        }
    }

    static void dda_display(String[][] a, int r, int c, String display) {
        System.out.print(display);
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }

    static void dda_display(String[][] a, int r, int c, String display, String separator) {
        System.out.print(display);
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                System.out.print(a[i][j] + "" + separator);
            }
            System.out.println();
        }
    }
}

import java.util.Scanner;
import java.util.StringTokenizer;

public class Special_Words {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the String: ");
        String n = in.nextLine();
        StringTokenizer st = new StringTokenizer(n, " ", false);
        while (st.hasMoreElements()) {
            System.out.println(st.nextToken());
        }
    }
}
